package thmw.ui;

import thmw.ui.*;

import javafx.fxml.FXML;

import javafx.stage.Stage;



public class Controller {



    @FXML
    private javafx.scene.control.Button closeButton;
    @FXML
    private javafx.scene.control.Button buttonLeftPVP;

    @FXML
    private void onClose(){
        // get a handle to the stage
        Stage stage = (Stage) closeButton.getScene().getWindow();
        // do what you have to do
        stage.close();
    }
    @FXML
    private void onPVP(){
        onClose();
        TheDrakeApp main=new TheDrakeApp();
        try {
            main.start(new Stage());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}

