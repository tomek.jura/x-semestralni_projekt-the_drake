package thmw.thedrake;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class TroopTile implements Tile, JSONSerializable {

    private Troop troop;
    private PlayingSide side;
    private TroopFace face;

    public TroopTile(Troop troop, PlayingSide side, TroopFace face) {

        this.troop = troop;
        this.side = side;
        this.face = face;

    }

    public PlayingSide side() {
        return side;
    }

    public TroopFace face() {
        return face;
    }

    public Troop troop() {
        return troop;
    }

    @Override
    public boolean canStepOn() {
        return false;
    }

    @Override
    public boolean hasTroop() {
        return true;
    }

    @Override
    public List<Move> movesFrom(BoardPos pos, GameState state) {
        List<Move> moves = new ArrayList<>();
        for(TroopAction action : troop.actions(face)) {
            moves.addAll(action.movesFrom(pos, side, state));
        }

        return moves;
    }

    public TroopTile flipped() {
        return new TroopTile(troop, side, face == TroopFace.AVERS ? TroopFace.REVERS : TroopFace.AVERS);
    }

    @Override
    public void toJSON(PrintWriter writer) {
        writer.print("\"troop\":\"" + troop + "\"," +
                     "\"side\":\"" + side + "\"," +
                     "\"face\":\"" + face + "\"");
    }
}
