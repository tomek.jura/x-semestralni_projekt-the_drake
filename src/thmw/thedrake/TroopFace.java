package thmw.thedrake;

public enum TroopFace {

    AVERS("AVERS"), REVERS("REVERS");

    private String value;

    TroopFace(String value) { this.value = value; }

    @Override
    public String toString() { return value; }
}
