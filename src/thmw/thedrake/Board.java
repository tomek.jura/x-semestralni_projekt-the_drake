package thmw.thedrake;

import java.io.PrintWriter;
import java.util.Arrays;

public class Board implements JSONSerializable{

    private BoardTile[][] desk;
    private int dimension;
	
	public Board(int dimension) {

        this.desk = new BoardTile[dimension][dimension];
        this.dimension = dimension;

        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                this.desk[i][j] = BoardTile.EMPTY;
            }
        }
    }

	public int dimension() {
		return dimension;
	} 
	
	public BoardTile at(BoardPos pos) {
		return desk[pos.i()][pos.j()];
	}

	public Board withTiles(TileAt ...ats) {

	    Board newBoard = this.copy();

        for (TileAt tileAt: ats) {
            newBoard.desk[tileAt.pos.i()][tileAt.pos.j()] = tileAt.tile;
        }

        return newBoard;
	}

	private Board copy() {

		Board newBoard = new Board(dimension);
		for(int i = 0; i < dimension; i++) {
			newBoard.desk[i] = Arrays.copyOf(desk[i], dimension);
		}

		return newBoard;
	}
	
	public PositionFactory positionFactory() {
		return new PositionFactory(dimension);
	}

	public static class TileAt {
		public final BoardPos pos;
		public final BoardTile tile;
		
		public TileAt(BoardPos pos, BoardTile tile) {
			this.pos = pos;
			this.tile = tile;
		}
	}

	@Override
	public void toJSON(PrintWriter writer) {
		writer.print("\"board\":{" + "\"dimension\":" + dimension + "," + "\"tiles\":[");
		boolean flag = false;
		for(int i = 0; i < dimension; i++) {
			for(int j = 0; j < dimension; j++) {
				if (flag) writer.print(",");
				flag = true;
				desk[j][i].toJSON(writer);
			}
		}
		writer.print("]}");
	}
}

