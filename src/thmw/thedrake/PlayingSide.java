package thmw.thedrake;

public enum PlayingSide {

    ORANGE("ORANGE"), BLUE("BLUE");

    private String value;

    PlayingSide(String value) { this.value = value; }

    @Override
    public String toString() { return value; }
}
